<?php namespace Clearweb\LocalLifeModels;

use Illuminate\Database\Eloquent\Model;

class Municipality extends Model
{
    protected $table = 'municipalities';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'image'
    ];


    public function getActions()
    {
        $actions = [];
        
        foreach($this->shops as $shop) {
            if (isset($shop->action->id))
                $actions[] = $shop->action;
        }
        
        foreach($this->restaurants as $restaurant) {
            if (isset($restaurant->action->id))
                $actions[] = $restaurant->action;
        }
        
        return $actions;
    }
    
    public function shops()
    {
        return $this->hasManyThrough('Clearweb\LocalLifeModels\Shop', 'Clearweb\LocalLifeModels\Address');
    }

    public function restaurants()
    {
        return $this->hasManyThrough('Clearweb\LocalLifeModels\Restaurant', 'Clearweb\LocalLifeModels\Address');
    }

    public function events()
    {
        return $this->hasManyThrough('Clearweb\LocalLifeModels\Event', 'Clearweb\LocalLifeModels\Address');
    }

    public function news()
    {
        return $this->hasMany('Clearweb\LocalLifeModels\NewsItem');
    }
    
    public function expert()
    {
        return $this->hasOne('Clearweb\LocalLifeModels\Expert');
    }

    public function addresses()
    {
        return $this->hasMany('Clearweb\LocalLifeModels\Address');
    }
}