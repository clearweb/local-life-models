<?php namespace Clearweb\LocalLifeModels;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'name',
        'image',
        'image2',
        'image3',
        'description',
        'address_id',
        'date'
    ];
    
    public function address()
    {
        return $this->belongsTo('Clearweb\LocalLifeModels\Address');
    }
    
    public function action()
    {
        return $this->morphOne('Clearweb\LocalLifeModels\Action', 'actionable');
    }
}