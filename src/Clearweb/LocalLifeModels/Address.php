<?php namespace Clearweb\LocalLifeModels;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table    = 'addresses';   
    protected $fillable = [
        'municipality_id',
        'street',
        'number',
        'extension',
        'postcode',
        'city',
        'country',
    ];
    
    public function municipality()
    {
        return $this->belongsTo('Clearweb\LocalLifeModels\Municipality');
    }
    
    public function shop()
    {
        return $this->hasOne('Clearweb\LocalLifeModels\Shop');
    }
}