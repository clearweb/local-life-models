<?php namespace Clearweb\LocalLifeModels;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    
    protected $fillable = [
        'actionable_id',
        'actionable_type',
        'title',
        'image',
        'description'
    ];
    
    public function actionable()
    {
        return $this->morphTo();
    }
    
}