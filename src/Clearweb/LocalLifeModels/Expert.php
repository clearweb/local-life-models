<?php namespace Clearweb\LocalLifeModels;

use Illuminate\Database\Eloquent\Model;

class Expert extends Model
{
    protected $fillable = [
        'municipality_id',
        'user_id',
        
        'first_name',
        'last_name',
        'image',
        'description',
        'tip',
        
        'facebook',
        'twitter',
        'instagram',
        'linkedin'
    ];
    
    public function municipality()
    {
        return $this->belongsTo('Clearweb\LocalLifeModels\Municipality');
    }
    
    public function getTitle()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}