<?php namespace Clearweb\LocalLifeModels;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $fillable = [
        'name',
        'phone',
        'website',
        'image',
        'image2',
        'image3',
        'description',
        'address_id',
        'subscribed',
        'opening_times',
    ];
    
    public function address()
    {
        return $this->belongsTo('Clearweb\LocalLifeModels\Address');
    }
    
    public function action()
    {
        return $this->morphOne('Clearweb\LocalLifeModels\Action', 'actionable');
    }
}