<?php namespace Clearweb\LocalLifeModels;

use Illuminate\Database\Eloquent\Model;

class NewsItem extends Model
{
    protected $table = 'news';
    
    protected $fillable = [
        'name',
        'image',
        'description',
        'municipality_id',
        'date'
    ];
    
    public function municipality()
    {
        return $this->belongsTo('Clearweb\LocalLifeModels\Municipality');
    }
    
}